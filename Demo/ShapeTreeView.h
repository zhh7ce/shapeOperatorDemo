// ShapeTreeView.h : CShapeTreeView 类的接口
//

#pragma once
#include "DemoDoc.h"
#include "ShapeDlg.h"


class CShapeTreeView : public CView {
protected: // 仅从序列化创建
	CShapeTreeView();
	DECLARE_DYNCREATE(CShapeTreeView)

	// 特性
public:
	CDemoDoc* GetDocument() const;

	// 操作
public:
	COperator* m_o;
	CShapeDlg* m_shape_dlg;

	// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	// 实现
public:
	virtual ~CShapeTreeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

	// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnEditShape();
	afx_msg void OnEditOperator();
};

#ifndef _DEBUG  // DemoView.cpp 中的调试版本
inline CDemoDoc* CShapeTreeView::GetDocument() const {
	return reinterpret_cast<CDemoDoc*>(m_pDocument);
}
#endif

