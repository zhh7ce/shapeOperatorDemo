// DemoView.cpp : CShapeTreeView 类的实现
//

#include "stdafx.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "Demo.h"
#endif

#include "DemoDoc.h"
#include "ShapeTreeView.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CShapeTreeView

IMPLEMENT_DYNCREATE(CShapeTreeView, CView)

BEGIN_MESSAGE_MAP(CShapeTreeView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_CREATE()
	ON_COMMAND(ID_1_32773, &CShapeTreeView::OnEditShape)
	ON_COMMAND(ID_Menu, &CShapeTreeView::OnEditOperator)
END_MESSAGE_MAP()

// CShapeTreeView 构造/析构

CShapeTreeView::CShapeTreeView() {
	// TODO: 在此处添加构造代码

}

CShapeTreeView::~CShapeTreeView() {}

BOOL CShapeTreeView::PreCreateWindow(CREATESTRUCT& cs) {
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CShapeTreeView 绘制

void CShapeTreeView::OnDraw(CDC* /*pDC*/) {
	CDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CShapeTreeView 打印

BOOL CShapeTreeView::OnPreparePrinting(CPrintInfo* pInfo) {
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CShapeTreeView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/) {
	// TODO: 添加额外的打印前进行的初始化过程
}

void CShapeTreeView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/) {
	// TODO: 添加打印后进行的清理过程
}


// CShapeTreeView 诊断

#ifdef _DEBUG
void CShapeTreeView::AssertValid() const {
	CView::AssertValid();
}

void CShapeTreeView::Dump(CDumpContext& dc) const {
	CView::Dump(dc);
}

CDemoDoc* CShapeTreeView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDemoDoc)));
	return (CDemoDoc*)m_pDocument;
}
#endif //_DEBUG


// CShapeTreeView 消息处理程序



int CShapeTreeView::OnCreate(LPCREATESTRUCT lpCreateStruct) {
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	CDemoDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CRect rt;
	GetClientRect(&rt);

	pDoc->m_shape_tree = new CTreeCtrl();

	pDoc->m_shape_tree->Create(WS_VISIBLE | WS_TABSTOP | WS_CHILD | WS_BORDER |
		TVS_HASBUTTONS | TVS_LINESATROOT | TVS_HASLINES |
		TVS_DISABLEDRAGDROP | TVS_NOTOOLTIPS | TVS_EDITLABELS,
		rt, this, 4396);
	pDoc->m_tree_root = pDoc->m_shape_tree->InsertItem(_T("Shape"), TVI_ROOT, TVI_SORT);
	//hLine_root = hLine;
	//Eagles subitems second(no sorting )
	//pDoc->m_shape_tree->InsertItem(_T("Eagles"), pDoc->m_tree_root);
	//pDoc->m_shape_tree->InsertItem(_T("On the Border"), pDoc->m_tree_root);
	//shape_tree->InsertItem(_T("Hotel California"), hLine);
	//shape_tree->InsertItem(_T("The Long Run"), hLine);
	pDoc->m_shape_tree->ShowWindow(SW_SHOWNORMAL);

	return 0;
}


BOOL CShapeTreeView::PreTranslateMessage(MSG* pMsg) {
	// TODO: 在此添加专用代码和/或调用基类
	if (WM_RBUTTONDOWN == pMsg->message) {
		CDemoDoc* pDoc = GetDocument();
		ASSERT_VALID(pDoc);
		CPoint point;
		GetCursorPos(&point);//获得鼠标点击的位置
		this->ScreenToClient(&point);//转化为客户坐标

		UINT uFlags;
		HTREEITEM hItem = pDoc->m_shape_tree->HitTest(point, &uFlags);

		if ((hItem != NULL) && (TVHT_ONITEM & uFlags)) {
			POSITION pos = pDoc->m_operator_list.GetHeadPosition();
			COperator* o;

			HTREEITEM hCurItem = pDoc->m_shape_tree->GetChildItem(pDoc->m_tree_root);

			while (pos != NULL) {
				o = pDoc->m_operator_list.GetNext(pos);
				if (hCurItem == hItem) {
					m_o = o;
					CMenu menu;
					menu.LoadMenuW(IDR_MENU1);
					CMenu *pPopup = menu.GetSubMenu(0);
					ClientToScreen(&point);
					pPopup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
					return CView::PreTranslateMessage(pMsg);
				}
				hCurItem = pDoc->m_shape_tree->GetNextSiblingItem(hCurItem);
			}
		}
	} 
	return CView::PreTranslateMessage(pMsg);
}


void CShapeTreeView::OnEditShape() {
	// TODO: 在此添加命令处理程序代码
	if (m_o->m_object_type == 1) {
		m_shape_dlg = new CCubeDlg();
		m_shape_dlg->Create(IDD_CUBEDLG, this);
		m_shape_dlg->m_pDoc = GetDocument();
		m_shape_dlg->m_o = m_o;
		m_shape_dlg->CenterWindow();
		m_shape_dlg->ShowWindow(SW_NORMAL);
	} else if (m_o->m_object_type == 2) {
		m_shape_dlg = new CCylinderDlg();
		m_shape_dlg->Create(IDD_CYLINDERDLG, this);
		m_shape_dlg->m_pDoc = GetDocument();
		m_shape_dlg->m_o = m_o;
		m_shape_dlg->CenterWindow();
		m_shape_dlg->ShowWindow(SW_NORMAL);
	}
}


void CShapeTreeView::OnEditOperator() {
	// TODO: 在此添加命令处理程序代码
	m_o->m_visible = true;
	m_shape_dlg = new COperatorDlg();
	m_shape_dlg->Create(IDD_OPERATORDLG, this);
	m_shape_dlg->LoadPtr(m_o, GetDocument());
	//m_shape_dlg->m_pDoc = GetDocument();
	//m_shape_dlg->m_o = m_o;
	m_shape_dlg->CenterWindow();
	m_shape_dlg->ShowWindow(SW_NORMAL);
}
