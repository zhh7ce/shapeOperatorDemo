//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 Demo.rc 使用
//
#define IDR_MENU1                       4
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_DemoTYPE                    130
#define IDD_CUBEDLG                     310
#define IDD_DIALOG1                     312
#define IDD_CYLINDERDLG                 312
#define IDD_OPERATORDLG                 317
#define IDC_BUTTON1                     1000
#define IDC_CUBE_EDITX                  1001
#define IDC_CUBE_EDITY                  1002
#define IDC_CUBE_EDITZ                  1003
#define IDC_CYL_EDITR                   1004
#define IDC_CYL_EDITH                   1005
#define IDC_RADIO1                      1006
#define IDC_RADIO2                      1007
#define ID_OPENGL_CUBE                  32771
#define ID_OPENGL_CYLINDER              32772
#define ID_1_32773                      32773
#define ID_Menu                         32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        319
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
